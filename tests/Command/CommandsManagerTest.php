<?php

namespace Kisphp\Tests\Command;

use Kisphp\DatabaseBundle\Command\CommandsManager;

class DefaultControllerTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var array
     */
    protected $connection = [
        'user' => 'user',
        'password' => 'pass',
        'host' => 'localhost',
        'dbname' => 'dbname',
    ];

    /**
     * @var string
     */
    protected $rootDir = 'tmp/dir';

    public function testImportCommands()
    {
        $cm = new CommandsManager($this->connection, $this->rootDir);

        $this->assertSame(
            $this->getExpectedImportCommand(),
            $cm->getImportCommand()
        );
    }

    public function testExportCommands()
    {
        $cm = new CommandsManager($this->connection, $this->rootDir);

        $this->assertSame(
            $this->getExpectedExportCommand(),
            $cm->getExportCommand()
        );
    }

    /**
     * @return string
     */
    protected function getExpectedImportCommand()
    {
        $expected = vsprintf(
            '/usr/bin/mysql -u%s -p%s -h%s %s < %s/database.sql',
            array_merge($this->connection, [$this->rootDir])
        );
        return $expected;
    }

    /**
     * @return string
     */
    protected function getExpectedExportCommand()
    {
        $expected = vsprintf(
            '/usr/bin/mysqldump -u%s -p%s -h%s %s > %s/database.sql',
            array_merge($this->connection, [$this->rootDir])
        );
        return $expected;
    }
}
