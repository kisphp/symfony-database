# Symfony Database Import/Export 

## Installation

```sh
composer require kisphp/symfony-database:~1.0
```

Register new bundle in AppKernel.php file in your symfony2 project

```php
$bundles = array(
    ...
    new Kisphp\DatabaseBundle\KisphpDatabaseBundle(),
);
```

## Usage:

Import `database.sql` file

```sh
php app/console zed:database --import
```

Export current database to `database.sql` file

```sh
php app/console zed:database --export
```

# Changelog

### 1.1.0
- support symfony >= 2.8, 3.x
