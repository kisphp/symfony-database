<?php

namespace Kisphp\DatabaseBundle\Command;

class CommandsManager
{
    /**
     * @var array
     */
    protected $connection = [];

    /**
     * @var string
     */
    protected $rootDirectoryPath;

    /**
     * @param array $connection
     * @param string $rootDirectoryPath
     */
    public function __construct(array $connection, $rootDirectoryPath)
    {
        $this->connection = $connection;
        $this->rootDirectoryPath = $rootDirectoryPath;
    }

    /**
     * @return string
     */
    public function getImportCommand()
    {
        $command = vsprintf(
            '/usr/bin/mysql -u%s -p%s -h%s %s < %s/database.sql',
            $this->getParameters()
        );

        return $command;
    }

    /**
     * @return string
     */
    public function getExportCommand()
    {
        $command = vsprintf(
            '/usr/bin/mysqldump -u%s -p%s -h%s %s > %s/database.sql',
            $this->getParameters()
        );

        return $command;
    }

    /**
     * @return array
     */
    protected function getParameters()
    {
        $databaseCredentials = [
            $this->connection['user'],
            $this->connection['password'],
            $this->connection['host'],
            $this->connection['dbname'],
            $this->rootDirectoryPath,
        ];

        return $databaseCredentials;
    }
}
